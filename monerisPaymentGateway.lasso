<?lasso
/**!
    Data type to connect with, and handle transactions through, the 
    Moneris Payment Gateway API.

    Based on the existing PHP classes provided by Moneris at:
    https://github.com/Moneris/eCommerce-Unified-API-PHP/blob/master/mpgClasses.php

    @author Jordon Davidson <jordon@yourmarriagesuccess.ca>
    @version 1.0.0
    @url https://gitlab.com/jordonedavidson/moneris-payment-gateway

    This type relies on Brad Lindsay's http_response type which can be found at:
      https://github.com/bfad/Lasso-HTTP

    This type is used to handle transactions to the Moneris Payment API. It does
    not provide opinions about the results of the transaction. instead it is up
    to the implementation to read values from the error routines and the parseResponse
    variable to take action on the results. The error state will be set if the http
    request returns a status code of 400 or higher.
*/

define monerisPaymentGateway => type {
     //   Moneris API constants.
    data
        globals::map = map(
            'MONERIS_PROTOCOL' = 'https',
            'MONERIS_HOST' = 'www3.moneris.com', 
            'MONERIS_TEST_HOST' = 'esqa.moneris.com',
            'MONERIS_US_HOST' = 'esplus.moneris.com',
            'MONERIS_US_TEST_HOST' = 'esplusqa.moneris.com',
            'MONERIS_PORT' ='443',
            'MONERIS_FILE' = '/gateway2/servlet/MpgRequest',
            'MONERIS_US_FILE' = '/gateway_us/servlet/MpgRequest',
            'MONERIS_MPI_FILE' = '/mpi/servlet/MpiServlet',
            'MONERIS_MPI_2_FILE' = '/mpi2/servlet/MpiServlet',
            'MONERIS_US_MPI_FILE' = '/mpi/servlet/MpiServlet',
            'API_VERSION'  = 'Lasso 1.0.0',
            'CONNECT_TIMEOUT' = '20',
            'CLIENT_TIMEOUT' = '35'
        )
    //    Member variables to store internal settings.
    data
        logFile::string = '/logs/moneris.log',
        mpgError::map = map(
            'error' = false,
            'message' = '',
            'code' = 0
        ),
        debug::boolean = false
    
    //  Member variables used in the transactions
    data
        storeId::string,
        apiToken::string,
        testing::boolean = false,
        endpoint::string,
        public transaction::map = map(
            'order_id',
            'cust_id', 
            'amount', 
            'pan', 
            'expdate', 
            'crypt_type'
            ),
        public countryCode::string = 'CA',
        public cofInfo::map = map(
            'payment_indicator' = null,
            'payment_information' = null,
            'issuer_id' = null
        ),
        public custInfo::map = map(),
        public response,
        public headers

    /**!
        Constructor method
    */
    public onCreate(storeId::string, apiToken::string) => {
        #storeId->trim
        #apiToken->trim

        // Load defaults
        .'storeId' = #storeId
        .'apiToken' = #apiToken
        // Set up the default logging
        .setLogFile

        // Reset the error.
        .resetError
    }

    /**!
        Post the transaction to Moneris
    */
    public httpsPost() => {
        // Set the endpoint based on the current data.
        .setEndpoint

        // Headers must be saved as a thread variable for use in the include_url function.
        var('headers')
        local('requestBody' = '')

        #requestBody->append('
            <?xml version="1.0" encoding="UTF-8"?>
                <request>
               	    <store_id>' + .'storeId' +'</store_id>
               		<api_token>' + .'apiToken' +'</api_token>
                    ')
        #requestBody->append(.requestToXML)
        #requestBody->append('
            </request>
        ')

        .'debug' ? .writeToLog('details', #requestBody)

        protect => {
            handle_error => {
                .setError(error_msg, error_code)
            }
            .'response' = include_url(
                .'endpoint',
                -postParams=#requestBody,
                -retrieveMimeHeaders='headers'
            )

            // Save the headers
            .'headers' = http_response($headers)

            .'debug' ? .writeToLog('details', '\n' + string(.'headers'))

            // Test to make sure we got a success.
            if (400 <= .'headers'->statusCode) => {
                fail(.'headers'->statusCode, .'headers'->statusMsg)
            }
        }
    }

    /**!
        Parse the response into a map
    */
    public parseResponse() => {
        .'debug' ? .writeToLog('details', '\n' + string(.'response'))
        protect => {
            handle_error => {
                .setError(error_msg, error_code)
            }

            local(
                'out' = map(),
                'xmlDoc' = xml(.'response'),
                'receipt' = #xmlDoc->extract('//receipt/*')
            )

            #receipt->forEach => {
                #out->insert(#1->tagName = #1->nodeValue)
            }

            return #out
        }
    }

    /**!
        Convert the transaction to an XML string

        @return string 
    */
    public requestToXML() => {
        local(
            'xml' = '',
            'transactionKeys' = array(
                'order_id',
                'cust_id', 
                'amount', 
                'pan', 
                'expdate', 
                'crypt_type',
                'dynamic_descriptor', 
                'wallet_indicator', 
                'market_indicator', 
                'cm_id'
            )
        )

        // Open the transaction block with the type. (Should always be purchase)
        #xml->append('<' + .'transaction'->get('type') +'>')

        // Add the transaction information.
        with k in #transactionKeys do => {
            if (.'transaction'->find(#k)) => {
                #xml->append('
                    <' + #k + '>' + .'transaction'->get(#k) +'</' + #k + '>
                ')

            }
        }

        if (.'custInfo'->size) => {
            #xml->append(.custInfoToXml)
        }

        // Close the transaction block.
        #xml->append('</' + .'transaction'->get('type') +'>')

        return #xml
    }


    /**!
        Calculate the API endpoint from the memver variables
    */
    public setEndpoint() => {
        local(
            'hostID' = 'MONERIS' + .getProcessCountryCode + .getTestModeString + '_HOST',
            'pathID' = 'MONERIS' + .getProcessCountryCode + .getMPICode + '_FILE'
        )

        .'endpoint' = .'globals'->get('MONERIS_PROTOCOL') + '://' + 
            .'globals'->get(#hostID) + ':' +
            .'globals'->get('MONERIS_PORT') +
            .'globals'->get(#pathID)

        .writeToLog('details', 'The endpoint is ' + .'endpoint')
    }

    /**!
        Return the result if the transaction based on the code

        @param string code The Response code
        @return integer 1 if approved 0 if declined
    */
    public getTransactionResult(code::string) => {
        local(
            'approvedCodes' = staticarray(
                '000', '001', '002', '003',
                '004', '005', '006', '007',
                '008', '009', '010', '023',
                '024', '025', '026', '027',
                '028', '029'
            )
        )

        return (#approvedCodes->contains(#code) ? 1 | 0)
    }

    /* ---------------------------------------------------------------
        Customer information methods
     --------------------------------------------------------------- */
    
    /**!
        Set the Email in customer info.

        @param string email. The customer email address.

        @todo Add a robust email validatior.
    */
    public setEmail(email::string) => {
        .'custInfo'->insert('email' = #email)
    }

    /**!
        Set the customer's special instructions.

        @param string instructions. The customer's instructions.
    */
    public setInstructions(instructions::string) => {
        .'custInfo'->insert('instructions' = #instructions)
    }

    /**!
        Set the customer billing information.

        @param map billing The billing information map.
    */
    public setBilling(billing::map) => {
        .'custInfo'->insert('billing' = #billing)
    }

    /**!
        Set the customer shipping information.

        @param map billing The shipping information map.
    */
    public setShipping(shipping::map) => {
        .'custInfo'->insert('shipping' = #shipping)
    }

    /**!
        Set the items in the order.

        @param map item An item map to add to the items array.
    */
    public addItems(item::map) => {
        if ( .'custInfo'->contains('item')) => {
            // add the item to the array
            .'custInfo'->get('item')->insert(#item)
        
        else
            // Create the key and set it equal to the array.
            .'custInfo'->insert('item' = array(#item))
        }
    }

    /**!
        Convert the custInfo map into XML for the transaction request.

        @return string
    */
    public custInfoToXml() => {
        local('xml' = '<cust_info>')

        // Email.
        if (.'custInfo'->contains('email')) => {
            #xml->append('
                <email>'+ .'custInfo'->get('email') +'</email>
            ')
        }
        // Information.
        if (.'custInfo'->contains('email')) => {
            #xml->append('
                <instructions>'+ .'custInfo'->get('instructions') +'</instructions>
            ')
        }
        // Billing.
        if (.'custInfo'->contains('billing')) => {
            #xml->append('<billing>')
                .'custInfo'->get('billing')->forEachPair => {
                    #xml->append('
                        <'+ #1->first +'>'+ string(#1->second) +'</'+ #1->first +'>
                    ')
                }
            #xml->append('</billing>')
        }
        // Shipping.
        if (.'custInfo'->contains('shipping')) => {
            #xml->append('<shipping>')
                .'custInfo'->get('shipping')->forEachPair => {
                    #xml->append('
                        <'+ #1->first +'>'+ string(#1->second) +'</'+ #1->first +'>
                    ')
                }
            #xml->append('</shipping>')
        }
        // Items.
        if (.'custInfo'->contains('item')) => {
            .'custInfo'->get('item')->forEach => {
                #xml->append('<item>')
                #1->forEachPair => {
                    #xml->append('
                        <'+ #1->first +'>'+ string(#1->second) +'</'+ #1->first +'>
                    ')
                }
                #xml->append('</item>')
            }
        }

        #xml->append('</cust_info>')

        return #xml
    }

    /**!
        Set the debug status

        @param boolean status True or false. Default false
    */
    public setDebugStatus(status::boolean = false) => {
        .'debug' = boolean(#status)
    }

    /**!
        Set the testing status

        @param boolean status True or false. Default false
    */
    public setTestingStatus(status::boolean = false) => {
        .'testing' = boolean(#status)
    }

    /**!
        Load the transaction map.
        
        This method always appends/updates so fragments of the map may be sent
        in order to expand the map.

        @param map transactionMap The values to load into the transaction Map.
    */
    public setTransaction(transactionMap::map) => {
        //log_always(#transactionMap)

        #transactionMap->forEachPair => {
            .'transaction'->insert(#1)
        }
        //log_always(.'transaction')
    
        .'debug' ? .writeToLog('detail', '\n' + string(.'transaction')) 
        //log_always('Finished setting transaction')     
    }

    /**!
        Return the path of the logfile
    */
    public getLogfile() => {
        return .'logFile'
    }

    /**!
        Set the path of the logfile

        @param string path The path to the logfile
    */
    public setLogFile(path::string = '') => {
        #path->trim
        if (not #path->size) => {
            #path = .'logFile'
        }

        protect => {
            handle_error => {
                log_critical(error_msg)
                .setError(error_msg, error_code)
            }
            /*
                Test to see if the log file already exists.
                If not attempt to create it.
              */
            local('testFile' = file(#path))
            if (not #testFile->exists) => {
                #testFile->openWrite
            }

            // set the permissions
            #testfile->chmod('0666')
            
            .'logFile' = #path
        }
    }

    /**!
        Boolean: Return the error status
    */
    public isError() => {
        return .'mpgError'->get('error')
    }

    /**!
        String: Return current error message
    */
    public getErrorMessage() => {
        return .'mpgError'->get('message')
    }

    /**!
        Integer: Return current error code.
    */
    public getErrorCode() => {
        return .'mpgError'->get('code')
    }

    /**!
        Return the country code part of the host path.
    
        @return string '_US' for US processing. Blank for Canada.
    */
    private getProcessCountryCode() => {
        return .'countryCode' == 'US' ? '_US' | ''
    }

    /**!
        Return the test code part of the host path.
    
        @return string '_TEST' for test processing. Blank for live.
    */
    private getTestModeString() => {
        return .'testing' ? '_TEST' | ''
    }

    /**!
        Return the MPI part of the host path.
    
        @return string One of _MPI_2, _MPI, or blank.
    */
    private getMPICode() => {
        // Only handling statndard transactions for now.
        return ''
    }

    /**1
        Reset the error map to no error.
    */
    private resetError() => {
        .'mpgError'->insert('error' = false)
        .'mpgError'->insert('message' = '')
        .'mpgError'->insert('code' = 0)
    }
    
    /**!
        Set the values of the error map.

        @param string message The error message
        @param integer code The error code
    */
    private setError(message::string = '', code::integer = 0) => {
        //log_detail('in setError')
        .'mpgError'->insert('error' = true)
        .'mpgError'->insert('message' = #message)
        .'mpgError'->insert('code' = #code)
        
        // If debug, then log the error.
        if(.'debug')  => {
            local('logMessage' = .getErrorCode + ': ' +.getErrorMessage)
            .writeToLog('critical', #logMessage)
        }
    }

    /**!
        Write to the log file

        @param stirng logLevel One of 'critical, deprecated, detail'
        @param string message The message to be logged.
    */
    private writeToLog(logLevel::string = 'detail', message::string = '') => {
        // Sanity check for logLevel.
        if (not staticarray('critical', 'deprecated', 'detail')->contains(#logLevel)) => {
            #logLevel = 'detail'
        }

        protect => {
            handle_error => {
                log_critical('Could not write to log: (' + error_code + ') ' +error_msg)
            }
        
            // Construct the message with the current date
            local(
                'log' = file(.'logfile'),
                'logMessage' = date + ': ' + #logLevel + ' - ' + #message)

            // Write to the log
            #log->doWithClose => {
                #log->openAppend
                #log->writeString(#logMessage)
                #log->writeString("\n")
            }
        }
    }
}

?>
