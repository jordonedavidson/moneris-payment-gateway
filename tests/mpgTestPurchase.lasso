<?lasso
/*
    Make a purchase with the Lasso API integration.

    Runs through all purchase possibilities from $1.00 to $1.99 against the 
    Moneris test API to show all results.
*/

include('../monerisPaymentGateway.lasso')

// instaniate the type with the request variables storeId and apiToken.
local('mpg' = monerisPaymentGateway('store5', 'yesguy'))

// set debug and testing to true.
#mpg->setDebugStatus(true)
#mpg->setTestingStatus(true)

// Transaction variables
local(
    'txn' = map(
        'type' = 'purchase',
        'cust_id' = 'customer01',
        'order_id' = 'ord' + date_format(date, '%q'),
        'amount' = '1.02',
        'pan' = '4242424242424242',
        'expdate' = '2412',
        'crypt_type' = '7'
    ),
    'output' = '',
    'order_number' = string(date_format(date, '%q')),
    'response' = map(),
    'cc' = map(
        1 = '4242424242424242',
        0 = '5454545454545454'
    )
)

// set up the output table.

#output->append('
    <table border='1'>
        <thead>
            <tr>
                <th>#</th>
                <th>Date Time</th>
                <th>OrderID</th>
                <th>ReceiptID</th>
                <th>Transaction ID</th>
                <th>Amount</th>
                <th>Message</th>
                <th>Reference #</th>
                <th>Response Code<th>
            </th>
        </thead>
        <tbody>             
')


loop(-to=99, -from=0, -by=1) => {
    local('dollars' = 1 + decimal(loop_count)/100)
    // Set the transaction amount and order number
    #txn->insert('amount' = #dollars->asString(-precision=2))
    #txn->insert('order_id' = 'ord' + #order_number + '_' + string(loop_count))

    #txn->insert('pan' = #cc->get(loop_count%2))
    // Load up transaction
    #mpg->setTransaction(#txn)

    // Post the transaction.
    #mpg->httpsPost
    // Parse the response
    #response = #mpg->parseResponse

    protect => {
        handle_error => {^ 
            '<p>Error: ' + error_code + ' - ' +error_message + '</p>'
        ^}
        
        
        #output->append('
            <tr>
                <td>' + string(loop_count +1) +' </td>
                <td>' + #response->get('TransDate') + ' ' + #response->get('TransTime') +'</td>
                <td>' + #txn->get('order_id') +'</td>
                <td>' + #response->get('ReceiptId') +'</td>
                <td>' + #response->get('TransID') +'</td>
                <td>' + #response->get('TransAmount') +'</td>
                <td>' + #response->get('Message') +'</td>
                <td>' + #response->get('ReferenceNum') +'</td>
                <td>' + #response->get('ResponseCode') +'</td>
            </tr>
        ')
        
    }
}


#output->append('
        </tbody>
    </table>
')

// Show the results
#output
